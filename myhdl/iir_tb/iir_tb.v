`timescale 1ns/10ps
`default_nettype none

module iir_tb (/*AUTOARG*/) ;

   parameter INPUT_WIDTH = `INPUT_WIDTH;
   parameter NUM_STAGES = `NUM_STAGES;
   localparam OUTPUT_WIDTH = 32;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire signed [OUTPUT_WIDTH-1:0] o_filtered_data;// From DUT of iir.v
   wire signed [INPUT_WIDTH-1:0] o_sample;      // From DUT of iir.v
   wire                 o_sample_wr;            // From DUT of iir.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  i_clk;                  // To DUT of iir.v
   reg signed [INPUT_WIDTH-1:0] i_coef_a;       // To DUT of iir.v
   reg                  i_coef_a_wr;            // To DUT of iir.v
   reg signed [INPUT_WIDTH-1:0] i_coef_b;       // To DUT of iir.v
   reg                  i_coef_b_wr;            // To DUT of iir.v
   reg signed [INPUT_WIDTH-1:0] i_sample;       // To DUT of iir.v
   reg                  i_sample_wr;            // To DUT of iir.v
   // End of automatics

   iir #(.NUM_STAGES(NUM_STAGES),
         .INPUT_WIDTH(INPUT_WIDTH))
     DUT(/*AUTOINST*/
         // Outputs
         .o_sample_wr                 (o_sample_wr),
         .o_sample                    (o_sample),
         .o_filtered_data             (o_filtered_data),
         // Inputs
         .i_clk                       (i_clk),
         .i_sample_wr                 (i_sample_wr),
         .i_sample                    (i_sample));

   initial
   begin
      $to_myhdl(o_sample_wr,
                o_sample,
                o_filtered_data);
      $from_myhdl(i_clk,
                  i_sample_wr,
                  i_sample);
   end

   integer i;
   initial
   begin
      $dumpfile("Viir_tb.vcd");
      $dumpvars();
      for (i = 0; i < NUM_STAGES; i = i + 1 )
        $dumpvars(0, iir_tb.DUT.acc[i]);
   end

endmodule // iir_tb
