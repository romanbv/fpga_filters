set all_facs [list]
lappend all_facs "i_clk" "i_wr" "i_data" "o_wr" "o_data" "fsm_state_reg" "line_cnt_reg" "line_total_reg" "ram_re_cnt_reg" "sort_cnt_reg" "ram_re_addr_reg" "ram_wr_addr_reg" {window_tb.DUT.sp_ram_inst[0].ram.o_data} {window_tb.DUT.sp_ram_inst[1].ram.o_data} {window_tb.DUT.sp_ram_inst[2].ram.o_data} {window_tb.DUT.\buff_out_reg[0]} {window_tb.DUT.\buff_out_reg[1]} {window_tb.DUT.\buff_out_reg[2]} {window_tb.DUT.\buff_out_reg[3]} {window_tb.DUT.\buff_out_reg[4]} {window_tb.DUT.\buff_out_reg[5]} {window_tb.DUT.\buff_out_reg[6]} {window_tb.DUT.\buff_out_reg[7]} {window_tb.DUT.\buff_out_reg[8]}
#"rx_wr" "rx_data" "o_sample_wr" "o_coef_wr" "fir_top_tb.DUT.rxfilter_inst.buffer_index_reg" "top_tb.rxfilter.buffer_index_next" "INDEX_LAST" "coefs_cnt_reg" "filtered_data" "data_ready_reg" "tx_wr" "tx_busy" "tx_data" "top_tb.txfilter.buffer_index_reg" "INPUT_WIDTH" "INPUT_BYTES" "INPUT_UPPER_BITS"
# "run_tx" "coefs_loaded" "fir_coefs_in" "fir_data_in_en" "fir_coefs_in_en" "data_in" "coefs_in" "data_out_en" "fir_filtered_data_out" "fir_data_out_en" "buffer_out" "run_tx" "tx_stb" "tx_busy" "pointer_out" "tx_data"

set num_added [ gtkwave::addSignalsFromList $all_facs ]
puts "num signals added: $num_added"

# zoom full
gtkwave::/Time/Zoom/Zoom_Full
