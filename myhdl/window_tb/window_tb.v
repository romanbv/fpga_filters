`timescale 1ns/10ps
`default_nettype none

module window_tb (/*AUTOARG*/) ;
   parameter INPUT_WIDTH = `INPUT_WIDTH;
   parameter WINDOW_SIZE = `WINDOW_SIZE;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire signed [INPUT_WIDTH*WINDOW_SIZE-1:0] o_data;// From DUT of window.v
   wire                 o_wr;                   // From DUT of window.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  i_clk;                  // To DUT of window.v
   reg signed [INPUT_WIDTH-1:0] i_data;          // To DUT of window.v
   reg                  i_wr;                   // To DUT of window.v
   // End of automatics

   window #(.INPUT_WIDTH(INPUT_WIDTH), .WINDOW_SIZE(WINDOW_SIZE))
   DUT(/*AUTOINST*/
       // Outputs
       .o_data                   (o_data),
       .o_wr                     (o_wr),
       // Inputs
       .i_clk                    (i_clk),
       .i_wr                     (i_wr),
       .i_busy                   (0),
       .i_data                   (i_data));

   initial
   begin
      $to_myhdl(o_data,
                o_wr);
      $from_myhdl(i_clk,
                  i_wr,
                  i_data);
   end

   integer i;
   initial
   begin
      $dumpfile("Vwindow_tb.vcd");
      $dumpvars();
      for (i = 0; i < 9; i = i + 1 )
        $dumpvars(0, window_tb.DUT.buff_out_reg[i]);
   end

endmodule // window_tb
