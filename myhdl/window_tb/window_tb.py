import os, subprocess
import numpy as np
from random import randrange

from myhdl import Cosimulation, Simulation, Signal, delay, always, intbv, \
    StopSimulation, instances, instance, block

FILE_NAME = "window"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

@block
def window_tb():

    INPUT_WIDTH = 32
    WINDOW_SIZE = 9

    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)

    # Outputs
    o_data = Signal(intbv(0)[INPUT_WIDTH*WINDOW_SIZE:])
    o_wr = Signal(bool(0))

    # Inputs
    i_clk, i_wr = [Signal(bool(0)) for i in range(2)]
    i_data = Signal(intbv(0)[INPUT_WIDTH:])

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall -DINPUT_WIDTH={INPUT_WIDTH} -DWINDOW_SIZE={WINDOW_SIZE} ../../sp_ram.v ../../{FILE_NAME}.v {FILE_NAME}_tb.v"
    os.system(cmd)
    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_data=o_data,
                       o_wr=o_wr,
                       # Inputs
                       i_clk=i_clk,
                       i_wr=i_wr,
                       i_data=i_data)

    @always(delay(CLK_HALF_PERIOD))
    def driver():
        i_clk.next = not i_clk

    # data = (0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09)

    #data = list(np.zeros(5, dtype=np.int32)) + list(range(10)) + list(np.zeros(5, dtype=np.int32))
    IMG_WIDTH = 5
    # data = [0]*5 + list(range(25)) + [0]*5
    data = [0]*5 + [7, 8, 4, 5, 5, 5, 9, 4, 3, 8, 5, 2, 7, 2, 2, 6, 1, 9, 2, 4, 3, 2, 6, 9, 4] + [0]*5
    @instance
    def stimulus():

        yield delay(40)

        cnt = 0
        for i in range(len(data)):
            cnt = cnt + 1

            i_data.next = data[i];
            i_wr.next = 1;

            yield delay(20)

            i_wr.next = 0;

            yield delay(200)

            if cnt == IMG_WIDTH:
                cnt = 0
                yield delay(1000)

        yield delay(1000)

        raise StopSimulation

    return instances()

tb = window_tb()
tb.run_sim()

subprocess.run(["gtkwave", "-S", "window_tb.tcl", f"V{FILE_NAME}_tb.vcd"])
