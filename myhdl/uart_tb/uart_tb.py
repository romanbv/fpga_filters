import os, subprocess
import numpy as np
from uartsim import rs232_tx

from myhdl import Cosimulation, Simulation, Signal, delay, always, intbv, \
    StopSimulation, instances, instance, ConcatSignal, block

FILE_NAME = "uart"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

@block
def uart_tb():
    CLK_RATE = 50e6 # 50 Mhz
    BAUD_RATE = 115200
    CLK_PER_BIT =  10 #int(CLK_RATE / BAUD_RATE)
    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)

    # Outputs
    o_wr, o_break, o_parity_err, o_frame_err, o_ck_uart = [Signal(bool(0)) for i in range(5)]
    o_data = Signal(intbv(0)[8:])

    # Inputs
    i_clk = Signal(bool(0))
    i_uart_rx = Signal(bool(1))

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall -DCLK_PER_BIT={CLK_PER_BIT} ../../rxuart.v {FILE_NAME}_tb.v"
    os.system(cmd)

    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_wr=o_wr,
                       o_data=o_data,
                       o_break=o_break,
                       o_parity_err=o_parity_err,
                       o_frame_err=o_frame_err,
                       o_ck_uart=o_ck_uart,
                       # Inputs
                       i_clk=i_clk,
                       i_uart_rx=i_uart_rx)

    @always(delay(CLK_HALF_PERIOD))
    def clk_gen():
        i_clk.next = not i_clk

    @instance
    def stimulus():
        yield delay(CLK_PERIOD*CLK_PER_BIT*24) # Clear any initial break condition

        data = (0x01, 0x02)
        for i in data:
            tx_data = intbv(i)
            yield rs232_tx(i_uart_rx, tx_data, CLK_PER_BIT*CLK_PERIOD)

        yield delay(2000)

        raise StopSimulation

    return instances()

tb = uart_tb()
tb.run_sim()


subprocess.run(["gtkwave", "-S", "gtkwave.tcl", f"V{FILE_NAME}_tb.vcd"])
