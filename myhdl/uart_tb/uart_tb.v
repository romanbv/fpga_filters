`timescale 1ns/10ps
`default_nettype none

module uart_tb (/*AUTOARG*/) ;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 o_break;                // From DUT of rxuart.v
   wire                 o_ck_uart;              // From DUT of rxuart.v
   wire [7:0]           o_data;                 // From DUT of rxuart.v
   wire                 o_frame_err;            // From DUT of rxuart.v
   wire                 o_parity_err;           // From DUT of rxuart.v
   wire                 o_wr;                   // From DUT of rxuart.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  i_clk;                  // To DUT of rxuart.v
   wire                  i_reset;                // To DUT of rxuart.v
   wire [30:0]           i_setup;                // To DUT of rxuart.v
   reg                  i_uart_rx;              // To DUT of rxuart.v
   // End of automatics

   assign i_setup = `CLK_PER_BIT;
   assign i_reset = 1'b0;

   rxuart
   DUT(/*AUTOINST*/
       // Outputs
       .o_wr                            (o_wr),
       .o_data                          (o_data[7:0]),
       .o_break                         (o_break),
       .o_parity_err                    (o_parity_err),
       .o_frame_err                     (o_frame_err),
       .o_ck_uart                       (o_ck_uart),
       // Inputs
       .i_clk                           (i_clk),
       .i_reset                         (i_reset),
       .i_setup                         (i_setup[30:0]),
       .i_uart_rx                       (i_uart_rx));

   initial
   begin
      $to_myhdl(o_wr,
                o_data,
                o_break,
                o_parity_err,
                o_frame_err,
                o_ck_uart);
      $from_myhdl(i_clk,
                  i_uart_rx);
   end

   initial
   begin
      $dumpfile("Vuart_tb.vcd");
      $dumpvars();
   end


endmodule // uart_tb
