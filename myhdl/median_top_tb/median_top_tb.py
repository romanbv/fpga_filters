import os, subprocess, random
import numpy as np
from uartsim import rs232_tx

from myhdl import Cosimulation, block, Signal, instance, instances, \
    StopSimulation, always, delay, intbv

FILE_NAME = "median_top"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

@block
def median_top_tb():
    CLK_RATE = 50e6 # 50 Mhz
    BAUD_RATE = 115200
    CLK_PER_BIT = 20 #int(CLK_RATE / BAUD_RATE)
    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)
    INPUT_WIDTH = 32

    IMG_WIDTH = 7
    IMG_HEIGHT = 5

    # Outputs
    o_uart_tx = Signal(bool(0))

    # Inputs
    i_clk = Signal(bool(0))
    i_uart_rx = Signal(bool(1))

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall" + \
        f" -DSETUP={CLK_PER_BIT} -DINPUT_WIDTH={INPUT_WIDTH} -DIMG_HEIGHT={IMG_HEIGHT} -DIMG_WIDTH={IMG_WIDTH}" + \
        f" ../../{FILE_NAME}.v ../../sp_ram.v ../../median.v ../../window.v ../../bitonic_sort.v ../../rxuart.v ../../txuart.v ../../rxprep.v ../../txprep.v {FILE_NAME}_tb.v"
    os.system(cmd)

    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_uart_tx=o_uart_tx,
                       # Inputs
                       i_clk=i_clk,
                       i_uart_rx=i_uart_rx)

    @always(delay(CLK_HALF_PERIOD))
    def clk_gen():
        i_clk.next = not i_clk

    random.seed(1)
    data = []
    for i in range(IMG_WIDTH*IMG_HEIGHT):
        for i in range(4):
            if i == 3:
                data.append(random.randint(1, 9))
            else:
                data.append(0)

    data = [0]*4*IMG_WIDTH + data + [0]*4*IMG_WIDTH

    print(data)

    @instance
    def stimulus():
        yield delay(CLK_PERIOD*CLK_PER_BIT*24) # Clear any initial break condition

        tx_cnt = 0
        for i in range(len(data)):
            # if i > IMG_WIDTH*4-1:
            tx_cnt = tx_cnt + 1

            tx_data = intbv(data[i])
            yield rs232_tx(i_uart_rx, tx_data, CLK_PER_BIT*CLK_PERIOD)

            if tx_cnt == 4:
                # print(f"tx_cnt = {tx_cnt}")
                # print(f"data = {data[i]}")
                # print()
                tx_cnt = 0
                yield delay(100000)

        yield delay(80000)

        raise StopSimulation

    return instances()

tb = median_top_tb()
tb.run_sim()

subprocess.run(["gtkwave", "-S", "median_top_tb.tcl", f"V{FILE_NAME}_tb.vcd"])
