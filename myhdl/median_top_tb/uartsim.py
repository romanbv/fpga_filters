from myhdl import delay

def rs232_tx(tx, rx, duration=20):

    """ Simple rs232 transmitter procedure.
    tx -- serial output data
    rx -- input data byte to be transmitted
    duration -- CLK_RATE / BAUD_RATE * CLK_PERIOD
    """

    tx.next = 0
    yield delay(duration)

    for i in range(8):
        tx.next = rx[i]
        yield delay(duration)

    tx.next = 1
    yield delay(duration)
