`timescale 1ns/10ps
`default_nettype none

module median_top_tb (/*AUTOARG*/) ;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 o_uart_tx;              // From DUT of median_top.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  i_clk;                  // To DUT of median_top.v
   reg                  i_uart_rx;              // To DUT of median_top.v
   // End of automatics

   median_top #(.INPUT_WIDTH(`INPUT_WIDTH),
                .SETUP(`SETUP),
                .IMG_WIDTH(`IMG_WIDTH),
                .IMG_HEIGHT(`IMG_HEIGHT))
   DUT (/*AUTOINST*/
        // Outputs
        .o_uart_tx           (o_uart_tx),
        // Inputs
        .i_clk               (i_clk),
        .i_uart_rx           (i_uart_rx));

   initial
   begin
      $to_myhdl(o_uart_tx);
      $from_myhdl(i_clk,
                  i_uart_rx);
   end

   integer i;
   initial
   begin
      $dumpfile("Vmedian_top_tb.vcd");
      $dumpvars();
   end


endmodule // median_top_tb
