`timescale 1ns/10ps
`default_nettype none

module bitonic_sort_tb (/*AUTOARG*/) ;
   parameter INPUT_WIDTH = `INPUT_WIDTH;
   parameter OUTPUT_WIDTH = `OUTPUT_WIDTH;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 o_busy;                 // From DUT of bitonic_sort.v
   wire [INPUT_WIDTH-1:0] o_median;              // From DUT of bitonic_sort.v
   wire [INPUT_WIDTH*OUTPUT_WIDTH-1:0] o_sorted_data;// From DUT of bitonic_sort.v
   wire                 o_wr;                   // From DUT of bitonic_sort.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  i_clk;                  // To DUT of bitonic_sort.v
   reg [INPUT_WIDTH*9-1:0] i_data;               // To DUT of bitonic_sort.v
   reg                  i_reset;                // To DUT of bitonic_sort.v
   reg                  i_wr;                   // To DUT of bitonic_sort.v
   // End of automatics

   bitonic_sort #(.INPUT_WIDTH(INPUT_WIDTH), .OUTPUT_WIDTH(OUTPUT_WIDTH))
   DUT(/*AUTOINST*/
       // Outputs
       .o_wr               (o_wr),
       .o_sorted_data      (o_sorted_data),
       .o_median           (o_median),
       // Inputs
       .i_clk              (i_clk),
       .i_wr               (i_wr),
       .i_reset            (i_reset),
       .i_data             (i_data));

   initial
   begin
      $to_myhdl(o_sorted_data,
                o_median);
      $from_myhdl(i_clk,
                  i_wr,
                  i_reset,
                  i_data);
   end

   initial
   begin
      $dumpfile("Vbitonic_sort_tb.vcd");
      $dumpvars();
   end

endmodule // bitonic_sort_tb
