import os, subprocess
import numpy as np

from myhdl import Cosimulation, block, Signal, instance, instances, \
    StopSimulation, always, delay, intbv

FILE_NAME = "bitonic_sort"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

@block
def bitonic_sort_tb():
    INPUT_WIDTH = 8
    OUTPUT_WIDTH = 9

    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)

    # Outputs
    o_sorted_data = Signal(intbv(0)[INPUT_WIDTH*OUTPUT_WIDTH:])
    o_median = Signal(intbv(0)[INPUT_WIDTH:])

    # Inputs
    i_clk, i_wr, i_reset = [Signal(bool(0)) for i in range(3)]
    i_data = Signal(intbv(0)[INPUT_WIDTH*9:])

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall" + \
        f" -DINPUT_WIDTH={INPUT_WIDTH} -DOUTPUT_WIDTH={OUTPUT_WIDTH}" + \
        f" ../../{FILE_NAME}.v {FILE_NAME}_tb.v"
    os.system(cmd)

    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_sorted_data=o_sorted_data,
                       o_median=o_median,
                       # Inputs
                       i_clk=i_clk,
                       i_wr=i_wr,
                       i_reset=i_reset,
                       i_data=i_data)

    @always(delay(CLK_HALF_PERIOD))
    def clk_gen():
        i_clk.next = not i_clk

    data = (7, 8, 4, 5, 9, 4, 5, 2, 7)
    @instance
    def stimulus():
        yield delay(20)

        for i in range(len(data)):
            i_data.next[INPUT_WIDTH*i+INPUT_WIDTH:INPUT_WIDTH*i] = data[i]

        i_wr.next = 1

        yield delay(20)

        i_wr.next = 0

        print()
        print("input: ", end=" ")
        for i in range(9):
            print(i_data[INPUT_WIDTH*i+INPUT_WIDTH:INPUT_WIDTH*i], end=" ")
        print()

        print("output:", end=" ")
        for i in range(16):
            print(o_sorted_data[INPUT_WIDTH*i+INPUT_WIDTH:INPUT_WIDTH*i], end=" ")
        print()

        print(f"median: {o_median}")

        raise StopSimulation

    return instances()

tb = bitonic_sort_tb()
tb.run_sim()

#subprocess.run(["gtkwave", "-S", "gtkwave.tcl", f"V{FILE_NAME}_tb.vcd"])
