import os, subprocess
import numpy as np
from myhdl import Cosimulation, Simulation, Signal, delay, always, intbv, \
    StopSimulation, instances, instance

FILE_NAME = "fir"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

samples_np = np.fromfile('../../jupyter/fir_tb_input.bin', dtype=np.int32)
samples_intbv = samples_np.astype(intbv)

def fir_tb():

    # Parameters
    NUM_STAGES = 16
    INPUT_WIDTH = 32
    COEF_WIDTH = INPUT_WIDTH

    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)

    # Outputs
    o_sample_wr = Signal(bool(0))
    o_sample = Signal(intbv(0)[INPUT_WIDTH:].signed())
    o_filtered_data = Signal(intbv(0)[32:].signed())

    # Inputs
    i_clk, i_sample_wr = [Signal(bool(0)) for i in range(2)]
    i_sample = Signal(intbv(0)[INPUT_WIDTH:].signed())

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall " + \
    f"-DNUM_STAGES={NUM_STAGES} -DINPUT_WIDTH={INPUT_WIDTH} -DTESTBENCH " + \
    f"../../{FILE_NAME}.v {FILE_NAME}_tb.v"
    os.system(cmd)
    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_sample_wr=o_sample_wr,
                       o_sample=o_sample,
                       o_filtered_data=o_filtered_data,
                       # Inputs
                       i_clk=i_clk,
                       i_sample_wr=i_sample_wr,
                       i_sample=i_sample)

    @always(delay(CLK_HALF_PERIOD))
    def driver():
        i_clk.next = not i_clk

    fir_output = []
    @instance
    def stimulus():
        for i in range(2):
            yield i_clk.negedge

        for i in samples_intbv:
            i_sample.next = i
            i_sample_wr.next = 1
            yield i_clk.negedge
            i_sample_wr.next = 0
            yield i_clk.negedge

            fir_output.append(int(o_filtered_data))

        fir_output_np = np.array(fir_output).astype(np.int32)
        fir_output_np.tofile("../../jupyter/fir_tb_output.bin")

        raise StopSimulation

    return instances()

sim = Simulation(fir_tb())
sim.run()

subprocess.run(["gtkwave", "-S", "gtkwave.tcl", f"V{FILE_NAME}_tb.vcd"])
