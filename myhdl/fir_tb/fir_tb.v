`timescale 1ns/10ps
`default_nettype none

module fir_tb (/*AUTOARG*/) ;

   localparam NUM_STAGES = `NUM_STAGES;
   localparam INPUT_WIDTH = `INPUT_WIDTH;
   localparam COEF_WIDTH = INPUT_WIDTH;
   localparam OUTPUT_WIDTH = 32;

   wire signed [OUTPUT_WIDTH-1:0] o_filtered_data;
   wire signed [INPUT_WIDTH-1:0]  o_sample;
   wire                           o_sample_wr;

   reg                            i_clk;
   reg signed [COEF_WIDTH-1:0]    i_coef;
   reg                            i_coef_wr;
   reg signed [INPUT_WIDTH-1:0]   i_sample;
   reg                            i_sample_wr;

   fir #(// Parameters
         .NUM_STAGES         (NUM_STAGES),
         .INPUT_WIDTH        (INPUT_WIDTH))
   DUT (// Outputs
        .o_sample_wr         (o_sample_wr),
        .o_sample            (o_sample),
        .o_filtered_data     (o_filtered_data),
        // Inputs
        .i_clk               (i_clk),
        .i_sample_wr         (i_sample_wr),
        .i_sample            (i_sample));

   initial
   begin
      $to_myhdl(o_sample_wr,
                o_sample,
                o_filtered_data);
      $from_myhdl(i_clk,
                  i_sample_wr,
                  i_sample);
   end

   integer i;
   initial
   begin
      $dumpfile("Vfir_tb.vcd");
      $dumpvars();
      for (i = 0; i < `NUM_STAGES; i = i + 1 )
        $dumpvars(0, fir_tb.DUT.acc[i]);
   end

endmodule // fir_tb
