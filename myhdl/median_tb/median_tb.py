import os, subprocess
import numpy as np

from myhdl import Cosimulation, block, Signal, instance, instances, \
    StopSimulation, always, delay, intbv

samples_np = np.fromfile('../../jupyter/median_tb_input.bin', dtype=np.int32)
samples_intbv = samples_np.astype(intbv)

print(samples_np)

FILE_NAME = "median"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

@block
def median_tb():
    INPUT_WIDTH = 32
    WINDOW_SIZE = 9;
    IMG_WIDTH = 10;
    IMG_HEIGHT = 10;

    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)

    # Outputs
    o_wr = Signal(bool(0))
    o_median = Signal(intbv(0)[INPUT_WIDTH:])

    # Inputs
    i_clk, i_wr = [Signal(bool(0)) for i in range(2)]
    i_data = Signal(intbv(0)[INPUT_WIDTH:])

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall" + \
        f" -DINPUT_WIDTH={INPUT_WIDTH} -DWINDOW_SIZE={WINDOW_SIZE} -DIMG_WIDTH={IMG_WIDTH} -DIMG_HEIGHT={IMG_HEIGHT}" + \
        f" ../../sp_ram.v ../../bitonic_sort.v ../../window.v ../../{FILE_NAME}.v {FILE_NAME}_tb.v"
    os.system(cmd)

    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_wr=o_wr,
                       o_median=o_median,
                       # Inputs
                       i_clk=i_clk,
                       i_wr=i_wr,
                       i_data=i_data)

    @always(delay(CLK_HALF_PERIOD))
    def clk_gen():
        i_clk.next = not i_clk

    median_out = []
    @instance
    def stimulus():

        yield delay(50)

        cnt = 0
        line_cnt = 0
        for i in range(samples_intbv.size):
            # print(i)
            cnt = cnt + 1

            i_data.next = samples_intbv[i];
            i_wr.next = 1;

            yield delay(20)

            i_wr.next = 0;

            yield delay(200)

            if cnt == IMG_WIDTH:
                cnt = 0
                line_cnt = line_cnt + 1
                # print(f"line: {line_cnt}")


                for j in range(IMG_WIDTH):
                    # print(f"{i}:  waiting for negedge")
                    yield o_wr.negedge

                    if (line_cnt > 2):
                        median_out.append(int(o_median))
                        # print(o_median)

                yield delay(1000)



        median_out_np = np.array(median_out).astype(np.int32)
        median_out_np.tofile("../../jupyter/median_tb_output.bin")

        yield delay(1000)

        raise StopSimulation

    return instances()


tb = median_tb()
tb.run_sim()

subprocess.run(["gtkwave", "-S", "gtkwave.tcl", f"V{FILE_NAME}_tb.vcd"])

# gtkwave -S gtkwave.tcl Vmedian_tb.vcd
