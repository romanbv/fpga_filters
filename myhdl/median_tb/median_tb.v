`timescale 1ns/10ps
`default_nettype none

module median_tb (/*AUTOARG*/) ;
   parameter INPUT_WIDTH = `INPUT_WIDTH;
   parameter WINDOW_SIZE = `WINDOW_SIZE;
   parameter IMG_HEIGHT = `IMG_HEIGHT;
   parameter IMG_WIDTH = `IMG_WIDTH;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire [INPUT_WIDTH-1:0] o_median;              // From DUT of median.v
   wire                   o_wr;                   // From DUT of median.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                   i_clk;                  // To DUT of median.v
   reg [INPUT_WIDTH-1:0] i_data;                 // To DUT of median.v
   reg                   i_wr;                   // To DUT of median.v
   // End of automatics

   median #(/*AUTOINSTPARAM*/
            // Parameters
            .INPUT_WIDTH                 (INPUT_WIDTH),
            .WINDOW_SIZE                 (WINDOW_SIZE),
            .IMG_WIDTH                   (IMG_WIDTH),
            .IMG_HEIGHT                  (IMG_HEIGHT))

                                                       DUT (/*AUTOINST*/
                                                            // Outputs
                                                            .o_wr               (o_wr),
                                                            .o_median           (o_median),
                                                            // Inputs
                                                            .i_clk              (i_clk),
                                                            .i_wr               (i_wr),
                                                            .i_busy             (0),
                                                            .i_data             (i_data));

   initial
   begin
      $to_myhdl(o_wr,
                o_median);
      $from_myhdl(i_clk,
                  i_wr,
                  i_data);
   end

   initial
   begin
      $dumpfile("Vmedian_tb.vcd");
      $dumpvars();
   end

   genvar i, j;
   for (i = 0; i < 11; i = i + 1)
   begin
      for (j = 0; j < 16; j = j + 1)
      begin
         initial
         begin
            $dumpfile("Vmedian_tb.vcd");
            $dumpvars(0, median_tb.DUT.bitonic_sort_inst.data[i][j]);
         end
      end
   end


endmodule // median_tb
