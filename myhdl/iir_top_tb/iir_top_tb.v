`timescale 1ns/10ps
`default_nettype none

module iir_top_tb (/*AUTOARG*/) ;

   /*AUTOWIRE*/
   // Beginning of automatic wires (for undeclared instantiated-module outputs)
   wire                 o_uart_tx;              // From DUT of iir_top.v
   // End of automatics

   /*AUTOREGINPUT*/
   // Beginning of automatic reg inputs (for undeclared instantiated-module inputs)
   reg                  i_clk;                  // To DUT of iir_top.v
   reg                  i_uart_rx;              // To DUT of iir_top.v
   // End of automatics

   iir_top #(.SETUP(`SETUP),
             .NUM_STAGES(`NUM_STAGES),
             .INPUT_WIDTH(`INPUT_WIDTH))
         DUT(/*AUTOINST*/
             // Outputs
             .o_uart_tx               (o_uart_tx),
             // Inputs
             .i_clk                   (i_clk),
             .i_uart_rx               (i_uart_rx));

   initial
   begin
      $to_myhdl(o_uart_tx);
      $from_myhdl(i_clk,
                  i_uart_rx);
   end

   integer i;
   initial
   begin
      $dumpfile("Viir_top_tb.vcd");
      $dumpvars();
      for (i = 0; i < `NUM_STAGES; i = i + 1 )
      begin
         $dumpvars(0, iir_top_tb.DUT.iir_inst.acc[i]);
         // $dumpvars(0, iir_top_tb.DUT.iir_inst.coef_reg[i]);
      end
   end
endmodule // iir_top_tb
