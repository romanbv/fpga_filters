set all_facs [list]
lappend all_facs "i_clk" "rx_wr" "rx_data" "o_sample_wr" "o_coef_wr" "fir_top_tb.DUT.rxfilter_inst.buffer_index_reg" "top_tb.rxfilter.buffer_index_next" "INDEX_LAST" "coefs_cnt_reg" "filtered_data" "data_ready_reg" "tx_wr" "tx_busy" "tx_data" "top_tb.txfilter.buffer_index_reg" "INPUT_WIDTH" "INPUT_BYTES" "INPUT_UPPER_BITS"
# "run_tx" "coefs_loaded" "fir_coefs_in" "fir_data_in_en" "fir_coefs_in_en" "data_in" "coefs_in" "data_out_en" "fir_filtered_data_out" "fir_data_out_en" "buffer_out" "run_tx" "tx_stb" "tx_busy" "pointer_out" "tx_data"

set num_added [ gtkwave::addSignalsFromList $all_facs ]
puts "num signals added: $num_added"

# zoom full
gtkwave::/Time/Zoom/Zoom_Full
