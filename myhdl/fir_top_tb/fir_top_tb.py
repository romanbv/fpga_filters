import os, subprocess
import numpy as np
from uartsim import rs232_tx

from myhdl import Cosimulation, block, Signal, instance, instances, StopSimulation, always, delay, intbv

FILE_NAME = "fir_top"
subprocess.run(["rm", f"V{FILE_NAME}_tb", f"V{FILE_NAME}_tb.vcd"])

@block
def fir_top_tb():
    CLK_RATE = 50e6 # 50 Mhz
    BAUD_RATE = 115200
    CLK_PER_BIT = 10 #int(CLK_RATE / BAUD_RATE)
    CLK_PERIOD = 20
    CLK_HALF_PERIOD = int(CLK_PERIOD / 2)
    NUM_STAGES = 16
    INPUT_WIDTH = 32

    # Outputs
    o_uart_tx = Signal(bool(0))

    # Inputs
    i_clk = Signal(bool(0))
    i_uart_rx = Signal(bool(1))

    cmd = f"iverilog -o V{FILE_NAME}_tb -Wall" + \
        f" -DSETUP={CLK_PER_BIT} -DNUM_STAGES={NUM_STAGES} -DINPUT_WIDTH={INPUT_WIDTH} -DTESTBENCH" + \
        f" ../../{FILE_NAME}.v ../../fir.v ../../rxuart.v ../../txuart.v ../../rxprep.v ../../txprep.v {FILE_NAME}_tb.v"
    os.system(cmd)

    DUT = Cosimulation(f"vvp -m ./myhdl.vpi V{FILE_NAME}_tb",
                       # Outputs
                       o_uart_tx=o_uart_tx,
                       # Inputs
                       i_clk=i_clk,
                       i_uart_rx=i_uart_rx)

    @always(delay(CLK_HALF_PERIOD))
    def clk_gen():
        i_clk.next = not i_clk

    @instance
    def stimulus():
        yield delay(CLK_PERIOD*CLK_PER_BIT*24) # Clear any initial break condition

        data = (0x01, 0x02, 0x03, 0x04)
        for i in data:
            tx_data = intbv(i)
            yield rs232_tx(i_uart_rx, tx_data, CLK_PER_BIT*CLK_PERIOD)

        yield delay(20000)

        raise StopSimulation

    return instances()

tb = fir_top_tb()
tb.run_sim()

subprocess.run(["gtkwave", "-S", "fir_top.tcl", f"V{FILE_NAME}_tb.vcd"])
