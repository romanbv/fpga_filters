`timescale 1ns/10ps
`default_nettype none

module fir_top (o_uart_tx, i_clk, i_uart_rx);
   parameter SETUP = 434; // 115200 Baud, if clk @ 50MHz
   parameter NUM_STAGES = 16;
   parameter INPUT_WIDTH = 32;

   output wire o_uart_tx;
   input wire  i_clk;
   input wire  i_uart_rx;

   wire [INPUT_WIDTH-1:0] filter_i_sample;
   wire filter_i_sample_wr;
   wire [7:0] rx_data;
   wire [31:0] filtered_data;

   wire filter_o_sample_wr;
   wire tx_wr;
   wire tx_busy;
   wire [7:0] tx_data;

   wire rx_wr;
   wire tx_break;
   wire cts_n;
   wire [30:0] i_setup;

   assign i_setup = SETUP;
   assign cts_n = 1'b0;
   assign tx_break = 1'b0;

   rxuart receiver(.i_clk(i_clk),
                   .i_reset(1'b0),
                   .i_setup(i_setup),
                   .i_uart_rx(i_uart_rx),
                   .o_wr(rx_wr),
                   .o_data(rx_data),
                   .o_break(),
                   .o_parity_err(),
                   .o_frame_err(),
                   .o_ck_uart());

   rxprep #(.NUM_STAGES(NUM_STAGES),.INPUT_WIDTH(INPUT_WIDTH))
   rxprep_inst(.o_data(filter_i_sample),
               .o_sample_wr(filter_i_sample_wr),
               .i_clk(i_clk),
               .i_wr(rx_wr),
               .i_data(rx_data));

   fir #(.NUM_STAGES(NUM_STAGES),.INPUT_WIDTH(INPUT_WIDTH))
   fir_inst(.o_filtered_data(filtered_data),
            .o_sample_wr(filter_o_sample_wr),
            .i_clk(i_clk),
            .i_sample_wr(filter_i_sample_wr),
            .i_sample(filter_i_sample));

   txprep txprep_inst(.o_wr(tx_wr),
                      .o_data(tx_data),
                      .i_clk(i_clk),
                      .i_wr(filter_o_sample_wr),
                      .i_data(filtered_data),
                      .i_busy(tx_busy));

   txuart transmitter(.i_clk(i_clk),
                      .i_reset(),
                      .i_setup(i_setup),
                      .i_break(tx_break),
                      .i_wr(tx_wr),
                      .i_data(tx_data),
                      .i_cts_n(cts_n),
                      .o_uart_tx(o_uart_tx),
                      .o_busy(tx_busy));

endmodule
