`timescale 1ns/10ps
`default_nettype none
//`define OUTPUT_WIDTH 9
//define INPUT_WIDTH 8

module bitonic_sort (/*AUTOARG*/
   // Outputs
   o_wr, o_sorted_data, o_median,
   // Inputs
   i_clk, i_wr, i_data
   ) ;

// `ifdef INPUT_WIDTH
//    parameter INPUT_WIDTH = `INPUT_WIDTH;
// `else
//    parameter INPUT_WIDTH = 32;
// `endif

// `ifdef OUTPUT_WIDTH
//    parameter OUTPUT_WIDTH = `OUTPUT_WIDTH;
// `else
//    parameter OUTPUT_WIDTH = 9;
// `endif

   parameter INPUT_WIDTH = 8;
   parameter OUTPUT_WIDTH = 9;

   localparam MEDIAN = (OUTPUT_WIDTH - 1) / 2;
   localparam MAX_POSITIVE_VALUE = {1'b0, {INPUT_WIDTH-1{1'b1}}};

   output wire                                o_wr;
   output wire [INPUT_WIDTH*OUTPUT_WIDTH-1:0] o_sorted_data;
   output wire [INPUT_WIDTH-1:0] o_median;

   input wire                                 i_clk, i_wr;
   input wire [INPUT_WIDTH*9-1:0]             i_data;

   wire signed [INPUT_WIDTH-1:0]              data [0:10][0:15];
   reg signed [INPUT_WIDTH-1:0]               data_reg [0:10][0:15];
   wire signed [INPUT_WIDTH-1:0]              data_next [0:10][0:15];

   reg                                        o_wr_reg;
   wire                                       o_wr_next;

   always @(posedge i_clk)
     o_wr_reg = o_wr_next;

   assign o_wr_next = i_wr ? 1'b1 : 1'b0;
   assign o_wr = o_wr_reg;

   genvar i;
   generate

      for (i = 0; i < 16; i = i + 1)
      begin: gen_block
         // Sort: stage 1
         if (i == 0 || i == 4 || i == 8 || i == 12)
         begin // Ascending
            assign data[1][i] = (data[0][i] > data[0][i+1]) ? data[0][i+1] : data[0][i];
            assign data[1][i+1] = (data[0][i] > data[0][i+1]) ? data[0][i] : data[0][i+1];
         end
         else if (i == 2 || i == 6 || i == 10 || i == 14)
         begin //Descending
            assign data[1][i] = (data[0][i] < data[0][i+1]) ? data[0][i+1] : data[0][i];
            assign data[1][i+1] = (data[0][i] < data[0][i+1]) ? data[0][i] : data[0][i+1];
         end

         // Merge: stage 2
         if (i == 0 || i == 1 || i == 8 || i == 9)
         begin // Ascending
            assign data[2][i] = (data[1][i] > data[1][i+2]) ? data[1][i+2] : data[1][i];
            assign data[2][i+2] = (data[1][i] > data[1][i+2]) ? data[1][i] : data[1][i+2];
         end
         else if (i == 4 || i == 5 || i == 12 || i == 13)
         begin // Descending
            assign data[2][i] = (data[1][i] < data[1][i+2]) ? data[1][i+2] : data[1][i];
            assign data[2][i+2] = (data[1][i] < data[1][i+2]) ? data[1][i] : data[1][i+2];
         end

         // Sort: stage 3
         if (i == 0 || i == 2 || i == 8 || i == 10)
         begin
            assign data[3][i] = (data[2][i] > data[2][i+1]) ? data[2][i+1] : data[2][i];
            assign data[3][i+1] = (data[2][i] > data[2][i+1]) ? data[2][i] : data[2][i+1];
         end
         else if (i == 4 || i == 6 || i == 12 || i == 14)
         begin
            assign data[3][i] = (data[2][i] < data[2][i+1]) ? data[2][i+1] : data[2][i];
            assign data[3][i+1] = (data[2][i] < data[2][i+1]) ? data[2][i] : data[2][i+1];
         end

         // Sort: stage 4
         if (i == 0 || i == 1 || i == 2 || i == 3)
         begin
            assign data[4][i] = (data[3][i] > data[3][i+4]) ? data[3][i+4] : data[3][i];
            assign data[4][i+4] = (data[3][i] > data[3][i+4]) ? data[3][i] : data[3][i+4];
         end
         else if (i == 8 || i == 9 || i == 10 || i == 11)
         begin
            assign data[4][i] = (data[3][i] < data[3][i+4]) ? data[3][i+4] : data[3][i];
            assign data[4][i+4] = (data[3][i] < data[3][i+4]) ? data[3][i] : data[3][i+4];
         end

         /****************************************/

         // Sort: stage 5
         always @(posedge i_clk)
         begin
            if (i_wr)
              data_reg[5][i] <= data_next[5][i];
         end // always @ (posedge i_clk)


         if (i == 0 || i == 1 || i == 4 || i == 5)
         begin
            assign data_next[5][i] = (data[4][i] > data[4][i+2]) ? data[4][i+2] : data[4][i];
            assign data_next[5][i+2] = (data[4][i] > data[4][i+2]) ? data[4][i] : data[4][i+2];
         end
         else if (i == 8 || i == 9 || i == 12 || i == 13)
         begin
            assign data_next[5][i] = (data[4][i] < data[4][i+2]) ? data[4][i+2] : data[4][i];
            assign data_next[5][i+2] = (data[4][i] < data[4][i+2]) ? data[4][i] : data[4][i+2];
         end

         assign data[5][i] = data_reg[5][i];

         /****************************************/

         // Sort: stage 6
         if (i == 0 || i == 2 || i == 4 || i == 6)
         begin
            assign data[6][i] = (data[5][i] > data[5][i+1]) ? data[5][i+1] : data[5][i];
            assign data[6][i+1] = (data[5][i] > data[5][i+1]) ? data[5][i] : data[5][i+1];
         end
         else if (i == 8 || i == 10 || i == 12 || i == 14)
         begin
            assign data[6][i] = (data[5][i] < data[5][i+1]) ? data[5][i+1] : data[5][i];
            assign data[6][i+1] = (data[5][i] < data[5][i+1]) ? data[5][i] : data[5][i+1];
         end

         // Sort: stage 7
         if (i < 8)
         begin
            assign data[7][i] = (data[6][i] > data[6][i+8]) ? data[6][i+8] : data[6][i];
            assign data[7][i+8] = (data[6][i] > data[6][i+8]) ? data[6][i] : data[6][i+8];
         end

         // Sort: stage 8
         if (i < 4 || (i > 7 && i < 12))
         begin
            assign data[8][i] = (data[7][i] > data[7][i+4]) ? data[7][i+4] : data[7][i];
            assign data[8][i+4] = (data[7][i] > data[7][i+4]) ? data[7][i] : data[7][i+4];
         end

         // Sort: stage 9
         if (i == 0 || i == 1 || i == 4 || i == 5 || i == 8 || i == 9 || i == 12 || i == 13)
         begin
            assign data[9][i] = (data[8][i] > data[8][i+2]) ? data[8][i+2] : data[8][i];
            assign data[9][i+2] = (data[8][i] > data[8][i+2]) ? data[8][i] : data[8][i+2];
         end

         // Sort: stage 10
         if (i == 0 || i == 2 || i == 4 || i == 6 || i == 8 || i == 10 || i == 12 || i == 14)
         begin
            assign data[10][i] = (data[9][i] > data[9][i+1]) ? data[9][i+1] : data[9][i];
            assign data[10][i+1] = (data[9][i] > data[9][i+1]) ? data[9][i] : data[9][i+1];
         end

         if (i < OUTPUT_WIDTH)
         begin
            assign o_sorted_data[(i*INPUT_WIDTH+INPUT_WIDTH-1):(i*INPUT_WIDTH)] = data[10][i];
            assign o_median = data[10][MEDIAN];
         end

         if (i < 9)
           assign data[0][i] = i_data[(i*INPUT_WIDTH+INPUT_WIDTH-1):(i*INPUT_WIDTH)];
         else
           assign data[0][i] = MAX_POSITIVE_VALUE;
      end // block: gen_block

   endgenerate

endmodule // bitonic_sort
