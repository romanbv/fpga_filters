`timescale 1ns/10ps
`default_nettype none

module window (/*AUTOARG*/
   // Outputs
   o_data, o_wr,
   // Inputs
   i_clk, i_wr, i_busy, i_data
   ) ;

   parameter INPUT_WIDTH = 32;
   parameter WINDOW_SIZE = 9;
   parameter IMG_WIDTH = 5;
   parameter IMG_HEIGHT = 5;
   localparam IMG_SIZE = IMG_WIDTH * IMG_HEIGHT;
   parameter RAM_ADDR_WIDTH = $clog2(IMG_WIDTH + 2) < 4 ? 4 : $clog2(IMG_WIDTH + 2);

   output wire        [INPUT_WIDTH*WINDOW_SIZE-1:0] o_data;
   output wire                                      o_wr;

   input wire                                       i_clk;
   input wire                                       i_wr;
   input wire                                       i_busy;
   input wire signed [INPUT_WIDTH-1:0]              i_data;

   /****************************************/

   wire [INPUT_WIDTH-1:0]                           ram_data_out [2:0];
   wire                                             ram_we [2:0];
   wire [RAM_ADDR_WIDTH-1:0]                        ram_addr;

   genvar i;
   generate
      for (i = 0; i < 3; i = i + 1)
      begin: sp_ram_inst
         sp_ram #(.DATA_WIDTH(INPUT_WIDTH),
                  .ADDR_WIDTH(RAM_ADDR_WIDTH))
         ram (// Outputs
              .o_data(ram_data_out[i]),
              // Inputs
              .i_clk(i_clk),
              .i_we(ram_we[i]),
              .i_data(i_data),
              .i_addr(ram_addr));
      end
   endgenerate

   /****************************************/

   localparam [3:0]
     FSM_IDLE = 0,
     FSM_WRITE_TO_RAM = 1,
     FSM_NEW_LINE = 2,
     FSM_READ_FROM_RAM = 3,
     FSM_LOAD_BUFFER = 4,
     FSM_CHK_TX_STATUS = 5,
     FSM_SORT = 6;

   reg [3:0]                                        fsm_state_reg;
   reg [3:0]                                        fsm_state_next;

   always @(posedge i_clk)
     fsm_state_reg <= fsm_state_next;

   always @(*)
   begin
      fsm_state_next = fsm_state_reg;
      case (fsm_state_reg)
        FSM_IDLE:
          begin
             if (i_wr)
               fsm_state_next = FSM_WRITE_TO_RAM;
          end
        FSM_WRITE_TO_RAM:
          begin
             if (ram_wr_addr_reg == IMG_WIDTH)
               fsm_state_next = FSM_NEW_LINE;
             else
               fsm_state_next = FSM_IDLE;
          end
        FSM_NEW_LINE:
          begin
             fsm_state_next = FSM_READ_FROM_RAM;
          end
        FSM_READ_FROM_RAM:
          begin
             fsm_state_next = FSM_LOAD_BUFFER;
          end
        FSM_LOAD_BUFFER:
          begin
             if (ram_re_cnt_reg == 2)
               fsm_state_next = FSM_CHK_TX_STATUS;
             else
               fsm_state_next = FSM_READ_FROM_RAM;
          end
        FSM_CHK_TX_STATUS:
          begin
             if (!i_busy)
               fsm_state_next = FSM_SORT;
          end
        FSM_SORT:
          begin
             if (sort_cnt_reg == IMG_WIDTH - 1)
               fsm_state_next = FSM_IDLE;
             else
               fsm_state_next = FSM_READ_FROM_RAM;
          end
        default:
          fsm_state_next = FSM_IDLE;
      endcase
   end

   /****************************************/

   assign ram_addr = (fsm_state_reg == FSM_WRITE_TO_RAM) ? ram_wr_addr_reg : ram_re_addr_reg;

   /****************************************/

   assign o_wr = fsm_state_reg == FSM_SORT;

   /****************************************/

   reg [$clog2(IMG_WIDTH):0] ram_wr_addr_reg;
   reg [$clog2(IMG_WIDTH):0] ram_wr_addr_next;

   initial ram_wr_addr_reg = 1;
   always @(posedge i_clk)
     ram_wr_addr_reg <= ram_wr_addr_next;

   always @(*)
   begin
      ram_wr_addr_next = ram_wr_addr_reg;
      if (fsm_state_reg == FSM_WRITE_TO_RAM)
        ram_wr_addr_next = ram_wr_addr_reg + 1;
      else if (fsm_state_reg == FSM_NEW_LINE)
        ram_wr_addr_next = 1;
   end

   /****************************************/

   reg [2:0] line_cnt_reg;
   reg [2:0] line_cnt_next;

   initial line_cnt_reg = 0;
   always @(posedge i_clk)
     line_cnt_reg <= line_cnt_next;

   always @(*)
   begin
      line_cnt_next = line_cnt_reg;
      if (fsm_state_reg == FSM_NEW_LINE)
      begin
         if (line_cnt_reg == 2)
           line_cnt_next = 0;
         else
           line_cnt_next = line_cnt_reg + 1;
      end
   end

   /****************************************/

    assign ram_we[0] = (fsm_state_reg == FSM_WRITE_TO_RAM && line_cnt_reg == 0);
    assign ram_we[1] = (fsm_state_reg == FSM_WRITE_TO_RAM && line_cnt_reg == 1);
    assign ram_we[2] = (fsm_state_reg == FSM_WRITE_TO_RAM && line_cnt_reg == 2);

   /****************************************/

   reg [$clog2(IMG_WIDTH):0] ram_re_addr_reg;
   reg [$clog2(IMG_WIDTH):0] ram_re_addr_next;

   initial ram_re_addr_reg = 0;
   always @(posedge i_clk)
     ram_re_addr_reg <= ram_re_addr_next;

   always @(*)
   begin
      ram_re_addr_next = ram_re_addr_reg;
      if (fsm_state_reg == FSM_SORT)
        ram_re_addr_next = ram_re_addr_reg - 2;
      else if (fsm_state_reg == FSM_LOAD_BUFFER)
        ram_re_addr_next = ram_re_addr_reg + 1;
      else if (fsm_state_reg == FSM_IDLE)
        ram_re_addr_next = 0;
   end

   /****************************************/

   reg [$clog2(IMG_WIDTH):0] ram_re_cnt_reg;
   reg [$clog2(IMG_WIDTH):0] ram_re_cnt_next;

   initial ram_re_cnt_reg = 0;
   always @(posedge i_clk)
     ram_re_cnt_reg <= ram_re_cnt_next;

   always @(*)
   begin
      ram_re_cnt_next = ram_re_cnt_reg;
      if (fsm_state_reg == FSM_LOAD_BUFFER)
        ram_re_cnt_next = ram_re_cnt_reg + 1;
      else if (fsm_state_reg == FSM_SORT)
        ram_re_cnt_next = 0;
   end

   /****************************************/

   reg [$clog2(IMG_WIDTH):0] sort_cnt_reg;
   reg [$clog2(IMG_WIDTH):0] sort_cnt_next;

   initial sort_cnt_reg = 0;
   always @(posedge i_clk)
     sort_cnt_reg <= sort_cnt_next;

   always @(*)
   begin
      sort_cnt_next = sort_cnt_reg;
      if (fsm_state_reg == FSM_SORT)
        sort_cnt_next = sort_cnt_reg + 1;
      else if (fsm_state_reg == FSM_IDLE)
        sort_cnt_next = 0;
   end

   /****************************************/

   reg [INPUT_WIDTH-1:0] buff_out_reg [0:WINDOW_SIZE-1];
   reg [INPUT_WIDTH-1:0] buff_out_next [0:2];

   integer k;
   initial
   begin
      for (k = 0; k < 9; k = k + 1)
        buff_out_reg[k] = 0;
   end

   always @(posedge i_clk)
   begin
      buff_out_reg[ram_re_cnt_reg + 0] <= buff_out_next[0];
      buff_out_reg[ram_re_cnt_reg + 3] <= buff_out_next[1];
      buff_out_reg[ram_re_cnt_reg + 6] <= buff_out_next[2];
   end

   always @(*)
   begin
      buff_out_next[0] = buff_out_reg[ram_re_cnt_reg + 0];
      buff_out_next[1] = buff_out_reg[ram_re_cnt_reg + 3];
      buff_out_next[2] = buff_out_reg[ram_re_cnt_reg + 6];
      if (fsm_state_reg == FSM_LOAD_BUFFER)
      begin
         buff_out_next[0] = ram_data_out[0];
         buff_out_next[1] = ram_data_out[1];
         buff_out_next[2] = ram_data_out[2];
      end
   end

   /****************************************/

   genvar j;
   generate
      // 31:0
      // 63:32
      // 95:64
      // 127:96
      // 159:128
      // 191:160
      // 223:192
      // 255:224
      // 287:256
      for (j = 0; j < 9; j = j + 1)
      begin: output_wires
         assign o_data[(j*INPUT_WIDTH)+(INPUT_WIDTH-1):j*INPUT_WIDTH] = buff_out_reg[j];
      end
   endgenerate

endmodule // window
