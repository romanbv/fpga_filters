`timescale 1ns/10ps
`default_nettype none

module txprep (/*AUTOARG*/
   // Outputs
   o_wr, o_busy, o_data,
   // Inputs
   i_clk, i_wr, i_data, i_busy
   ) ;

   input wire i_clk;
   input wire i_wr;
   input wire [31:0] i_data;
   input wire i_busy;

   output wire o_wr;
   output wire o_busy;
   output wire [7:0] o_data;

   /****************************************/

   assign o_busy = fsm_state_reg != IDLE;

   /****************************************/
   reg [2:0] fsm_state_reg;
   reg [2:0] fsm_state_next;

   localparam [2:0]
     IDLE = 3'h0,
     START_DATA_TRANSMISSION = 3'h1,
     DETECT_TRANSMISSION_START = 3'h2,
     DETECT_TRANSMISSION_END = 3'h3,
     INCREASE_BUFFER_INDEX = 3'h4;

   always @(posedge i_clk)
     fsm_state_reg <= fsm_state_next;

   always @(*)
   begin
      fsm_state_next = fsm_state_reg;
      case (fsm_state_reg)
        IDLE:
          begin
             if (i_wr)
               fsm_state_next = START_DATA_TRANSMISSION;
          end
        START_DATA_TRANSMISSION:
          begin
             fsm_state_next = DETECT_TRANSMISSION_START;
          end
        DETECT_TRANSMISSION_START:
          begin
             if (i_busy)
               fsm_state_next = DETECT_TRANSMISSION_END;
          end
        DETECT_TRANSMISSION_END:
          begin
             if (!i_busy)
               fsm_state_next = INCREASE_BUFFER_INDEX;
          end
        INCREASE_BUFFER_INDEX:
          begin
             if (buffer_index_reg == 3)
               fsm_state_next = IDLE;
             else
               fsm_state_next = START_DATA_TRANSMISSION;
          end
        default: fsm_state_next = IDLE;
      endcase // case (fsm_state_reg)
   end // always @ (*)

   /****************************************/

   reg [1:0] buffer_index_reg;
   wire [1:0] buffer_index_next;

   initial buffer_index_reg = 2'h0;
   always @(posedge i_clk)
     if (fsm_state_reg == INCREASE_BUFFER_INDEX)
       buffer_index_reg <= buffer_index_next;

   assign buffer_index_next = buffer_index_reg + 2'h01;

   /****************************************/

   reg o_wr_reg;
   wire o_wr_next;

   always @(posedge i_clk)
     o_wr_reg <= o_wr_next;

   assign o_wr_next = (fsm_state_reg == START_DATA_TRANSMISSION);
   assign o_wr = o_wr_reg;

   /****************************************/

   wire [7:0] buffer [0:3];

   assign {buffer[0], buffer[1], buffer[2], buffer[3]} = i_data;

   /****************************************/

   reg [7:0] tx_data_reg;
   wire [7:0] tx_data_next;

   always @(posedge i_clk)
     tx_data_reg <= tx_data_next;

   assign tx_data_next = buffer[buffer_index_reg];
   assign o_data = tx_data_reg;

endmodule // tx_filter
