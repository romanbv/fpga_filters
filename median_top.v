`timescale 1ns/10ps
`default_nettype none

module median_top (i_clk, i_uart_rx, o_uart_tx);
   parameter SETUP = 434; // 115200 Baud, if clk @ 50MHz
   parameter INPUT_WIDTH = 32;
   parameter IMG_WIDTH = 512;
   parameter IMG_HEIGHT = 512;

   output wire o_uart_tx;
   input wire  i_clk;
   input wire  i_uart_rx;

   wire [30:0] i_setup;
   wire [INPUT_WIDTH-1:0] filter_i_sample;
   wire filter_i_sample_wr;
   wire [7:0] rx_data;
   wire [31:0] filtered_data;

   wire filter_o_sample_wr;
   wire tx_wr;
   wire tx_busy;
   wire tx_filter_busy;
   wire [7:0] tx_data;

   wire rx_wr;
   wire tx_break;
   wire cts_n;

   assign i_setup = SETUP;
   assign cts_n = 1'b0;
   assign tx_break = 1'b0;

   rxuart receiver(.i_clk(i_clk),
                   .i_reset(1'b0),
                   .i_setup(i_setup),
                   .i_uart_rx(i_uart_rx),
                   .o_wr(rx_wr),
                   .o_data(rx_data),
                   .o_break(),
                   .o_parity_err(),
                   .o_frame_err(),
                   .o_ck_uart());

   rxprep #(.INPUT_WIDTH(INPUT_WIDTH))
   rxprep_inst(.o_data(filter_i_sample),
               .o_sample_wr(filter_i_sample_wr),
               .i_clk(i_clk),
               .i_wr(rx_wr),
               .i_data(rx_data));

   median #(.INPUT_WIDTH(INPUT_WIDTH),
            .IMG_WIDTH(IMG_WIDTH),
            .IMG_HEIGHT(IMG_HEIGHT))
   median_inst(.o_wr(filter_o_sample_wr),
               .o_median(filtered_data),
               .i_clk(i_clk),
               .i_data(filter_i_sample),
               .i_wr(filter_i_sample_wr),
               .i_busy(tx_filter_busy));

   txprep txprep_inst(.o_wr(tx_wr),
                      .o_data(tx_data),
                      .o_busy(tx_filter_busy),
                      .i_clk(i_clk),
                      .i_wr(filter_o_sample_wr),
                      .i_data(filtered_data),
                      .i_busy(tx_busy));

   txuart transmitter(.i_clk(i_clk),
                      .i_reset(1'b0),
                      .i_setup(i_setup),
                      .i_break(tx_break),
                      .i_wr(tx_wr),
                      .i_data(tx_data),
                      .i_cts_n(cts_n),
                      .o_uart_tx(o_uart_tx),
                      .o_busy(tx_busy));

endmodule
