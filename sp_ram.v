`timescale 1ns/10ps
`default_nettype none

// Quartus Prime Verilog Template
// Single port RAM with single read/write address and initial contents
// specified with an initial block

module sp_ram
#(parameter DATA_WIDTH=8, parameter ADDR_WIDTH=6)
(
   input [(DATA_WIDTH-1):0]  i_data,
   input [(ADDR_WIDTH-1):0]  i_addr,
   input                     i_we, i_clk,
   output [(DATA_WIDTH-1):0] o_data
);

   // Declare the RAM variable
   reg [DATA_WIDTH-1:0]       ram[2**ADDR_WIDTH-1:0];

   // Variable to hold the registered read address
   reg [ADDR_WIDTH-1:0]       addr_reg;

   // Specify the initial contents.  You can also use the $readmemb
   // system task to initialize the RAM variable from a text file.
   // See the $readmemb template page for details.
   initial
   begin : INIT
	  integer i;
	  for(i = 0; i < 2**ADDR_WIDTH; i = i + 1)
		ram[i] = {DATA_WIDTH{1'b0}};
   end

   always @(posedge i_clk)
   begin
	  // Write
	  if (i_we)
		ram[i_addr] <= i_data;

	  addr_reg <= i_addr;
   end

   // Continuous assignment implies read returns NEW data.
   // This is the natural behavior of the TriMatrix memory
   // blocks in Single Port mode.
   assign o_data = ram[addr_reg];

endmodule
