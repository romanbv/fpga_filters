`timescale 1ns/10ps
`default_nettype none

module rxprep (/*AUTOARG*/
   // Outputs
   o_data, o_sample_wr,
   // Inputs
   i_clk, i_wr, i_data
   ) ;

   parameter NUM_STAGES = 2;
   parameter INPUT_WIDTH = 32;

   localparam INPUT_UPPER_BITS = 8 - INPUT_WIDTH % 8;
   localparam INPUT_BYTES = (INPUT_UPPER_BITS == 8) ? INPUT_WIDTH / 8 : INPUT_WIDTH / 8 + 1;
   localparam [1:0] INDEX_LAST = INPUT_BYTES;

   input wire                    i_clk;
   input wire                    i_wr;
   input wire [7:0]              i_data;

   output wire [INPUT_WIDTH-1:0] o_data;
   output reg                    o_sample_wr;

   /****************************************/

   reg [2:0] fsm_state_reg;
   reg [2:0] fsm_state_next;

   localparam [2:0]
     IDLE = 3'h00,
     LOAD_DATA_INTO_BUFFER = 3'h01,
     INCREASE_BUFFER_INDEX = 3'h02,
     RESET_BUFFER_INDEX = 3'h03,
     LOAD_SAMPLES = 3'h04;

   always @(posedge i_clk)
     fsm_state_reg <= fsm_state_next;

   always @(*)
   begin
      fsm_state_next = fsm_state_reg;
      o_sample_wr = 1'b0;

      case (fsm_state_reg)
        IDLE:
          begin
             if (i_wr)
               fsm_state_next = LOAD_DATA_INTO_BUFFER;
          end
        LOAD_DATA_INTO_BUFFER:
          begin
             fsm_state_next = INCREASE_BUFFER_INDEX;
          end
        INCREASE_BUFFER_INDEX:
          begin
             if (buffer_index_next == INDEX_LAST)
               fsm_state_next = RESET_BUFFER_INDEX;
             else
               fsm_state_next = IDLE;
          end
        RESET_BUFFER_INDEX:
          begin
             fsm_state_next = LOAD_SAMPLES;
          end
        LOAD_SAMPLES:
          begin
             fsm_state_next = IDLE;
             o_sample_wr = 1'b1;
          end
        default:
          fsm_state_next = IDLE;
      endcase
   end

   /****************************************/

   reg [1:0] buffer_index_reg;
   wire [1:0] buffer_index_next;

   initial buffer_index_reg = 2'h0;
   always @(posedge i_clk)
     if (fsm_state_reg == INCREASE_BUFFER_INDEX || fsm_state_reg == RESET_BUFFER_INDEX)
       buffer_index_reg <= buffer_index_next;

   assign buffer_index_next = (fsm_state_reg == RESET_BUFFER_INDEX) ? 2'h00 : buffer_index_reg + 2'h01;

   /****************************************/

   reg [7:0] buffer_reg [0:3];
   wire [7:0] buffer_next;

   always @(posedge i_clk)
     if (fsm_state_reg == LOAD_DATA_INTO_BUFFER)
       buffer_reg[buffer_index_reg] <= buffer_next;

   assign buffer_next = i_data;

   /****************************************/

   genvar i;
   generate
      for (i = 0; i != INPUT_BYTES; i = i + 1)
      begin: generate_buffer
         if (i == 0)
         begin
            if (INPUT_UPPER_BITS != 8)
              assign o_data[(INPUT_BYTES * 8) - 1 - INPUT_UPPER_BITS:(INPUT_BYTES * 8) - 8] = buffer_reg[i][INPUT_UPPER_BITS-1:0];
            else
              assign o_data[(INPUT_BYTES * 8) - 1:(INPUT_BYTES * 8) - 8] = buffer_reg[i][INPUT_UPPER_BITS-1:0];
         end
         else
         begin
            assign o_data[(INPUT_BYTES * 8) - i * 8 - 1:(INPUT_BYTES * 8) - (i + 1) * 8] = buffer_reg[i];
         end
      end
   endgenerate

endmodule
