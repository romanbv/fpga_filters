`timescale 1ns/10ps
`default_nettype none

module median (/*AUTOARG*/
   // Outputs
   o_wr, o_median,
   // Inputs
   i_clk, i_wr, i_busy, i_data
   ) ;

   parameter INPUT_WIDTH = 32;
   parameter WINDOW_SIZE = 9;
   parameter IMG_WIDTH = 512;
   parameter IMG_HEIGHT = 512;

   output wire                               o_wr;
   output wire signed [INPUT_WIDTH-1:0]      o_median;

   input wire                                i_clk;
   input wire                                i_wr;
   input wire                                i_busy;
   input wire signed [INPUT_WIDTH-1:0]       i_data;


   wire [INPUT_WIDTH-1:0]                    median;
   wire                                      window_en;
   wire                                      sort_en;
   wire [INPUT_WIDTH*WINDOW_SIZE-1:0]        windowed_data;
   wire                                      sorting_complete;


   assign window_en = i_wr;

   window #(// Parameters
            .INPUT_WIDTH (INPUT_WIDTH),
            .WINDOW_SIZE (WINDOW_SIZE),
            .IMG_WIDTH   (IMG_WIDTH),
            .IMG_HEIGHT  (IMG_HEIGHT))
   window_inst (// Outputs
                .o_data             (windowed_data),
                .o_wr               (sort_en),
                // Inputs
                .i_clk              (i_clk),
                .i_wr               (window_en),
                .i_busy             (i_busy),
                .i_data             (i_data[INPUT_WIDTH-1:0]));

   bitonic_sort #(// Parameters
                  .INPUT_WIDTH (INPUT_WIDTH))
   bitonic_sort_inst (// Outputs
                      .o_wr                (o_wr),
                      .o_sorted_data       (),
                      .o_median            (o_median),
                      // Inputs
                      .i_clk               (i_clk),
                      .i_wr                (sort_en),
                      .i_data              (windowed_data));
endmodule // median
