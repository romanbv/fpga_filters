`timescale 1ns/10ps
`default_nettype none

module iir (/*AUTOARG*/
   // Outputs
   o_sample_wr, o_sample, o_filtered_data,
   // Inputs
   i_clk, i_sample, i_sample_wr,
   ) ;

   parameter NUM_STAGES = 5;
   parameter INPUT_WIDTH = 32;
   localparam OUTPUT_WIDTH = 32;

   localparam PRODUCT_WIDTH = INPUT_WIDTH * 2;
   localparam ACC_WIDTH = PRODUCT_WIDTH - 4;
   localparam ROUNDING_REQUIRED = (ACC_WIDTH > OUTPUT_WIDTH) ? 1'b1 : 1'b0;

   input wire                            i_clk;
   input wire                            i_sample_wr;
   input wire signed [INPUT_WIDTH-1:0]   i_sample;

   output wire                           o_sample_wr;
   output wire signed [INPUT_WIDTH-1:0]  o_sample;
   output wire signed [OUTPUT_WIDTH-1:0] o_filtered_data;

   wire signed [PRODUCT_WIDTH-1:0]       product_a [0:NUM_STAGES-1];
   wire signed [PRODUCT_WIDTH-1:0]       product_b [0:NUM_STAGES-1];

   reg signed [INPUT_WIDTH-1:0]          sample_reg [0:NUM_STAGES-1];

   reg signed [INPUT_WIDTH-1:0]          feedback_reg [0:NUM_STAGES-1];
   reg signed [INPUT_WIDTH-1:0]          coef_a_reg [0:NUM_STAGES-1];
   reg signed [INPUT_WIDTH-1:0]          coef_b_reg [0:NUM_STAGES-1];
   wire signed [ACC_WIDTH-1:0]           acc [0:NUM_STAGES-1];

   initial
   begin
`ifdef TESTBENCH
      $readmemh("../../jupyter/iir_coefs_b.mem", coef_b_reg);
      $readmemh("../../jupyter/iir_coefs_a.mem", coef_a_reg);
`else
      $readmemh("jupyter/iir_coefs_b.mem", coef_b_reg);
      $readmemh("jupyter/iir_coefs_a.mem", coef_a_reg);
`endif
   end

   integer j;
   initial
   begin
      for (j = 0; j < NUM_STAGES; j = j + 1)
      begin
         sample_reg[j] = 0;
         feedback_reg[j] = 0;
      end
   end

   genvar i;
   generate
      for (i = 0; i < NUM_STAGES; i = i + 1)
      begin: main_for

         if (i == 0)
         begin
            always @(posedge i_clk)
            begin
               if (i_sample_wr)
                 sample_reg[0] <= i_sample;
            end

            always @(*)
              feedback_reg[0] = o_filtered_data;

            assign product_b[0] = sample_reg[0] * coef_b_reg[0];
            assign acc[0] = product_b[0];
         end

         if (i != 0)
         begin
            always @(posedge i_clk)
            begin
               if (i_sample_wr)
               begin
                  sample_reg[i] <= sample_reg[i-1];
                  feedback_reg[i] <= feedback_reg[i-1];
               end
            end

            assign product_b[i] = coef_b_reg[i] * sample_reg[i];
            assign product_a[i] = coef_a_reg[i] * feedback_reg[i];
            assign acc[i] = acc[i-1] + product_b[i] - product_a[i];
         end
      end
   endgenerate

   assign o_sample_wr = i_sample_wr;

   generate
      if (ROUNDING_REQUIRED)
        assign o_filtered_data = acc[NUM_STAGES-1][ACC_WIDTH-1:ACC_WIDTH-OUTPUT_WIDTH];
      else
        assign o_filtered_data = acc[NUM_STAGES-1];
   endgenerate

endmodule
